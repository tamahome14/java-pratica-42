
import utfpr.ct.dainf.if62c.pratica.Elipse;
import utfpr.ct.dainf.if62c.pratica.Circulo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jorge
 */
public class Pratica42 {
    public static void main(String[] args) {
        Circulo circulo = new Circulo(5);
        Elipse elipse = new Elipse(5,3);
        System.out.println(circulo.getArea());
        System.out.println(circulo.getPerimetro());
        System.out.println(elipse.getArea());
        System.out.println(elipse.getPerimetro());
    }
}
